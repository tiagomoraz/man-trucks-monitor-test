import React, { useState, useEffect, useRef } from 'react';
import './Filters.scss';
import { usePrevious } from '../../utils.js';


const Filters = ({ handleSearch }) => {
	const [canSubmit, setCanSubmit] = useState(false);
	const [formValues, setFormValues] = useState({
		licensePlate: '',
		type: '',
		radius: ''
	});
	const prevFormValues = usePrevious(formValues);
	const inputRef = useRef(null);

	
	const handleFieldValueChange = event => {
		const { name, value } = event.target;

		setFormValues({
			...formValues,
			[name]: value
		});
	}

	useEffect(() => {
		const { licensePlate, type, radius } = formValues;
		const isValid = (licensePlate !== '' && type !== '');

		if( prevFormValues && (prevFormValues.licensePlate !== licensePlate || prevFormValues.type !== type
					|| prevFormValues.radius !== radius) ) {
			setCanSubmit(isValid);
		}
	}, [formValues, prevFormValues]);

	useEffect(() => {
		inputRef.current.focus();
	}, [inputRef]);

	const licensePlateRegex = '[A-Za-z0-9]{2}-[A-Za-z0-9]{2}-[A-Za-z0-9]{2}';

	const handleSubmit = (e, values) => {
		e.preventDefault();

		if( canSubmit ) {
			setCanSubmit(false);
			handleSearch(values);
		}
	}

	return (
		<section className="SearchFilters">
			<form className="SearchFilters__form" onSubmit={(e) => handleSubmit(e, formValues)}>
				<input ref={inputRef} name="licensePlate" type="text" placeholder="Search by license plate" value={formValues['licensePlate']}
					required pattern={licensePlateRegex} onChange={handleFieldValueChange} />

				<select name="type" value={formValues['type']} required onChange={handleFieldValueChange}>
					<option value="" disabled>Select POI type</option>
					<option value="all">View All</option>
					<option value="gas_station">Gas Stations</option>
					<option value="restaurant">Restaurants</option>
					<option value="lodging">Hotels</option>
				</select>

				<select name="radius" value={formValues['radius']} onChange={handleFieldValueChange}>
					<option value="" disabled>Select radius</option>
					<option value="2000">2 km</option>
					<option value="5000">5 km</option>
					<option value="10000">10 km</option>
					<option value="20000">20 km</option>
				</select>

				<button disabled={!canSubmit} type="submit">Search</button>
			</form>
		</section>
	);
};

export default Filters;