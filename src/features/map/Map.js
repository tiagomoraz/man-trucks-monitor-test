import React from 'react';
import { fetchData } from '../../services/api';
import { getMiddleElement, getIcon } from '../../utils.js';
import Fitlers from '../filters/Filters';
import currentLocationIcon from '../../assets/images/icn-current-location.png';
import timerIcon from '../../assets/images/time.png';
import shopLocationIcon from '../../assets/images/shop-location.png';


const Map = ({ mapRef, service }) => {
	const { map, marker, setMapSize, event, dirRendered, dirService, infoWindow, latLngBounds } = service;
	let currentTruckPosition = null;
	let historyLocationRoute = null;
	const markersArray = [];
	const circleIcon = window.google.maps.SymbolPath.CIRCLE;

	const calculateAndDisplayRoute = ({ start, end }) => {
	  const request = {
	  	origin: start,
	    destination: end,
	    travelMode: window.google.maps.TravelMode['DRIVING']
	  };

	  dirService.route(request, function(result, status) {
	    if (status === 'OK') {
	    	dirRendered.setMap(null);
	    	dirRendered.setDirections({routes: []});

	    	let bounds = new latLngBounds();
	    	bounds.extend(start);
	    	bounds.extend(end);
	    	map.fitBounds(bounds);	    	

	    	dirRendered.setOptions({
	    		suppressMarkers: true,
	    		geodesic: true,
	    		polylineOptions: {
	    			strokeColor: '#4285F4',
	    			fillColor: '#4285F4',
    				strokeWeight: 5,
	    			icons: [
				      {
				        icon: {
				        	path: circleIcon
				        },
				        offset: "100%"
				      }
				    ]
	    		},
	    	});

	    	dirRendered.setDirections(result);
	    	dirRendered.setMap(map);

	    	const { distance, duration, steps } = result.routes[0].legs[0];
	    	const template = (`<div class="content">
		    						<p>
			    						<img src="${timerIcon}" alt="timer" />
			    						<b>${duration.text}</b>
			    					</p>
			    					<p>
			    						<img src="${shopLocationIcon}" alt="shop-location" />
			    						<span>${distance.text}</span>
			    					</p>
		    					</div>`);

	    	infoWindow.setContent('');
	    	infoWindow.setContent(template);
	    	infoWindow.setPosition(getMiddleElement(steps).start_location);
	        infoWindow.open(map);
	    }
	  });
	}

	const createMarker = (place, icon = '', zIndex = 1) => {
		const newMarker = new marker({
	    map,
	    position: place.geometry.location,
	    icon: {
	    	url: icon,
	    	scaledSize: new setMapSize(40, 40)
	    },
	    zIndex
	  });

	  markersArray.push(newMarker);

	  new event.addListener(newMarker, "click", () => calculateAndDisplayRoute({ start: currentTruckPosition, end: place.geometry.location }, newMarker ));
	}

	const clearPOIsMarkers = () => {
		if( markersArray.length ) {
			markersArray.forEach(marker => marker.setMap(null));
		}
	}

	const clearRoutes = () => {
		dirRendered.setMap(null);
	  dirRendered.setDirections({routes: []});
	}

	const createTruckCurrentPosMarker = position => {
		const payload = {
			geometry: { location: position }
		};

		createMarker(payload, currentLocationIcon, 2);
	}

	const setTruckLatestPos = history => {
		if( !history.length ) return null;

		const path = history.map(({ lat, lng }) => ({ lat, lng }));

		historyLocationRoute = new window.google.maps.Polyline({
			path,
			geodesic: true,
			strokeColor: "#000000",
			strokeOpacity: 0,
			icons: [
				{
					icon: {
						path: circleIcon,
						fillOpacity: 1,
						fillColor: '#fff',
						strokeOpacity: 1,
						strokeColor: "#000",
						strokeWeight: 4,
						scale: 8,
						zIndex: 5
					},
					offset: "0px",
					clickable: false
				},
				{
					icon: {
						path: circleIcon,
						fillOpacity: .7,
						zIndex: 3,
						scale: 6
					},
					repeat: "20px",
					clickable: false
				}
			],
		});

	  historyLocationRoute.setMap(map);
	}

	const handleNearBySearch = (results, status, type) => {
		if (status === window.google.maps.places.PlacesServiceStatus.OK) {
			let bounds = new latLngBounds();

			results.map(place => {
				bounds.extend(place.geometry.location);
				return createMarker(place, getIcon(type));
			});

			map.fitBounds(bounds);
	  }
	};

	const handlePlacesRequest = (params, location) => {
		const typeOptions = ['gas_station', 'restaurant', 'lodging'];
		const selectedPOIs = params.get('type') === 'all' ? typeOptions : [params.get('type')];
		const DEFAULT_RADIUS = '50000';
	  const placesService = new window.google.maps.places.PlacesService(map);

	  selectedPOIs.forEach(type => {
  		const request = {
		    location,
		    radius: params.get('radius') || DEFAULT_RADIUS,
		    type: [type]
		  };

	    placesService.nearbySearch(request, (results, status) => handleNearBySearch(results, status, type));
	  });
	}

	const handleSearch = values => {
		const params = new URLSearchParams(values);
		const url = `./fleet/trucks?${params.toString()}&opennow`;
		
		fetchData(url).then(response => {
			if( response.status === 200 ) {
				const { data } = response;

				const { lat, lng } = data.info.locations[data.info.locations.length-1];
				const location = { lat, lng };

				handlePlacesRequest(params, location);

				clearPOIsMarkers();
				clearRoutes();
				infoWindow.setContent('');
				infoWindow.close();

				if( historyLocationRoute ) historyLocationRoute.setMap(null);
				currentTruckPosition = location;
				createTruckCurrentPosMarker(location);
				setTruckLatestPos(data.info.locations);
			}
		}).catch(error => console.error(error));
	};

	return (
		<>
			<Fitlers handleSearch={handleSearch} />
			<div id="google-map" ref={mapRef} style={{ width: '100%', height: '100%', minHeight: '100vh' }} />
		</>
	);
};

export default Map;