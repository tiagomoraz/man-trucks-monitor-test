import React, { useRef, useState, useEffect, useCallback } from 'react';
import { fetchData } from '../../services/api';
import Map from '../map/Map';

const TruckMonitor = () => {
	const googleMapRef = useRef(null);
	const [loading, setLoading] = useState(false);
	const [apiKey, setApiKey] = useState('');
	const [googleService, setGoogleService] = useState(null);
	const [errors, setErrors] = useState(null);

	const fetchApiKey = useCallback(async () => {
		const url = './credentials/apiKey';

		setLoading(true);

		try {
			const result = await fetchData(url);

			if( !result.data ) {
				throw new Error('An error occurred fetching the API key.');	
			}

			setApiKey(result.data.key);
			setErrors(null); 
		} catch (error) {
			console.error(error);
			setErrors(error);
		} finally {
			setLoading(false);
		}
	}, []);

	const handleMapCreation = useCallback(() => {
		if( window.google && !googleService ) {
			const { Marker, Size, event, DirectionsRenderer, DirectionsService, InfoWindow, LatLngBounds } = window.google.maps;

			setGoogleService({
				marker: Marker,
				setMapSize: Size,
				event,
				dirRendered: new DirectionsRenderer(),
				dirService: new DirectionsService(),
				infoWindow: new InfoWindow(),
				latLngBounds: LatLngBounds
			});
		}
	}, [googleService]);

	useEffect(() => {
		if( googleService && !googleService.map ) {
			const defaultMapProps = {
				zoom: 16,
				center: {
					lat: 38.72545,
					lng: -9.14990,
				},
				disableDefaultUI: true,
			};

			setGoogleService({
				...googleService,
				map: new window.google.maps.Map(googleMapRef.current, defaultMapProps)
			});
			
			googleService.dirRendered.setMap(googleService.map);
		}
	}, [googleService])

	useEffect(() => {
		if( !apiKey && !loading && !errors) {
			fetchApiKey();
		}
	}, [apiKey, loading, fetchApiKey, errors]);

	useEffect(() => {
		if( apiKey && !window.google ) {
			const googleMapScript = document.createElement('script');
			googleMapScript.type = 'text/javascript';
			googleMapScript.defer = 'defer';
		  googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&libraries=places`;
			window.document.body.appendChild(googleMapScript);

    	googleMapScript.addEventListener('load', handleMapCreation);
		}
	}, [apiKey, handleMapCreation]);
 
	return (
		<>
			{(loading || !googleService)
				? <p>loading...</p>
				: <Map mapRef={googleMapRef} service={googleService} />
			}
		</>
	);
};

export default TruckMonitor;