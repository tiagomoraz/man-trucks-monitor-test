import React from 'react';
import './App.scss';

import TruckMonitor from './features/truck-monitor/TruckMonitor';

function App() {
  return (
    <div className="App">
      <TruckMonitor />
    </div>
  );
}

export default App;
