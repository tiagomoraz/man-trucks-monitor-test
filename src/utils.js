import { useEffect, useRef } from 'react';
import gasStationIcon from './assets/images/icn-gas-station.png';
import restaurantIcon from './assets/images/icn-restaurant.png';
import hotelIcon from './assets/images/icn-hotel.png';

export function usePrevious(value) {
	const ref = useRef();

	useEffect(() => {
		ref.current = value;
	}, [value]);

	return ref.current;
}

export function getMiddleElement(arr) {
	if( !arr.length ) return [];

	if( arr.length > 1 ) {
		const selectedStep = arr.length % 2 === 0 ? arr.length - 1 : arr.length;

		return arr[Math.round(selectedStep / 2)];
	}

	return arr[0];
}

export function getIcon(name) {
	if( !name ) return;
	
	const icons = {
		'gas_station': gasStationIcon,
		'restaurant': restaurantIcon,
		'lodging': hotelIcon
	};

	return icons[name];
}