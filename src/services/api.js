import axios from 'axios';

export const fetchData = url => (
	axios.get(url)
		.then(response => response)
		.catch(error => error)
);