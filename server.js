const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const morgan = require('morgan');


const app = express();
const port = process.env.PORT || 3001;
const monk = require('monk');
const db = monk('localhost:27017/MANtruck');

morgan(':method :url :status :res[content-length] - :response-time ms');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req,res,next){
    req.db = db;
    next();
});

app.get('/credentials/apiKey', function (req, res) {
	const collection = db.get('Auth');
  	
  	collection.find({ name: 'apiKey' }, {}, function(e, result) {
  		console.log(result[0])
        res.json(result[0]);
    });
});

app.get('/fleet/trucks', (req,res) => {
	const params = {...req.query};
	const collection = db.get('MANtruckcollection');

    collection.find({ license_plate: params.licensePlate.toUpperCase() }, {}, function(e, result) {
        res.json(result[0]);
    });
});

app.listen(port, () => console.log(`Listening on port: ${port}`));