This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) template.

## Available Scripts

In the project directory, you can run:

### `npm server`

Runs the server
It will be served on (http://localhost:3001).


### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `Instalation`
	
	Lets start by running `npm install` or 'yarn' if you prefer.
	

	## Set up MongoDB

	This project uses MongoDB (https://www.mongodb.com/), to set it up in your machine, follow this instructions: https://docs.mongodb.com/manual/installation/

	## You will need to start a cluster in order to run the following commands.
		running `mongo` should be enough to make you start, otherwise, follow the instruction helping section.

	After installing MongoDB, we'll need to create and update the db with our data, for that, we do:

	 - Create a db 
	 	`use MANtruck` (to make the db connection to work, you need to strictly follow the naming of dbs and collecitons )

	 - Insert data into `MANtruckcollection` collection. 
	 	`db.MANtruckcollection.insertMany([
			{
				license_plate: "32-34-RH",
				brand: "MAN",
				model: "34cr44",
				serial_id: "999",
				info: {
					driver: {
						Name: "Tiago Moreira",
						age: 26,
						license_number: '3849382',
					},
					locations: [{
						lat: 38.713918,
						lng: -9.123376,
					},
					{
						lat: 38.717970,
						lng: -9.119041,
					},
					{
						lat: 38.719611,
						lng: -9.120876,
					}]
				}
			},
			{
				license_plate: "86-LV-17",
				brand: "MAN",
				model: "00dko93",
				serial_id: "039322",
				info: {
					driver: {
						Name: "John Doe",
						age: 44,
						license_number: '28928134',
					},
					locations: [{
						lat: 38.719578,
						lng: -9.116338,
					},
					{ 
						lat: 38.722675,
						lng: -9.121241,
					},
					{
						lat: 38.726709,
						lng: -9.123161,
					}]
				}
			}
		])`

	- Insert data into `Auth` collection. (You will need a valid google maps api key: [https://developers.google.com/maps/documentation/javascript/get-api-key])
	`db.Auth.insert({ key: 'API_KEY', name: 'apiKey' })`


After this, you should be ready to go!

Run `npm run server` and on another tab `npm start` to start the application.